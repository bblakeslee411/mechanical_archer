MechArcher
==========

Mechanical Archer project from the Archery Club at RIT.

A full project report can be found in Blakeslee_Robotic_Archer_Summary.pdf.